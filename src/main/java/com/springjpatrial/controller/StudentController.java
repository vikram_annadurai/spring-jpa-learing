package com.springjpatrial.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springjpatrial.model.Student;
import com.springjpatrial.service.StudentService;

@RestController
@RequestMapping("/v1")
public class StudentController {

	@Autowired
	StudentService studentService;

	@PostMapping("/student/post")
	public Object create(@RequestBody Student data) {
		return studentService.create(data);
	}

	@GetMapping("/student/get")
	public List<Student> getall() {
		return studentService.getall();
	}
//
//	@PutMapping("/student/update/{id}")
//	public Student update(@PathVariable int id, @RequestBody Student data) {
//		return studentService.update(id, data);
//	}

//	@DeleteMapping("/student/delete/{id}")
//	public Object delete(@PathVariable int id) {
//		return studentService.delete(id);
//	}

}
