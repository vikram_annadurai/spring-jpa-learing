package com.springjpatrial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.springjpatrial.repository.StudentRepository;

@SpringBootApplication
@ComponentScan(basePackages= {"com.springjpatrial.controller","com.springjpatrial.service","com.springjpatrial.dao","com.springjpatrial.repository"})
public class SpringJpaTrialApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaTrialApplication.class, args);
	}

}
