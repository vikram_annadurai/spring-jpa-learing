package com.springjpatrial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springjpatrial.dao.StudentDao;
import com.springjpatrial.model.Student;

@Service
public class StudentService {

	@Autowired
	StudentDao studentDao;

	public Student create(Student data) {
		return studentDao.createStudent(data);
	}

	public List<Student> getall() {
		return studentDao.getAllStudent();
	}

//	public Object delete(int id) {
//		return studentDao.deleteStudent(id);
//	}


//	public Student update(int id, Student data) {
//		return studentDao.updateStudent(id, data);
//	}

}