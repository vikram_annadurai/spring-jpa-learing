package com.springjpatrial.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springjpatrial.model.Student;
import com.springjpatrial.repository.StudentRepository;

@Component
public class StudentDao {
	
	@Autowired 
	StudentRepository<Student> student;

	public Student createStudent(Student data) {
		return (Student) student.save(data);
	}

	public List<Student> getAllStudent() {
		// TODO Auto-generated method stub
		return student.findAll();
	}
	
	public Object findStudent(long studentid) {
		return student.findById(studentid).get();
	}

//	public Object deleteStudent(int id) {
//		return student.deleteById(id);
//	}

//	@SuppressWarnings("unchecked")
//	public Optional<Object> updateStudent( int id, Student data) {
//		return student.findById(id).map(stu -> {
//        stu.setStudentid(data.getStudentid());
//        stu.setStudentname(data.getStudentname());
//        stu.setStudentage(data.getStudentage());
//        return student.save(stu);
//    });}
	
	

}
